// Express
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

// SQLite
const sqlite3 = require('sqlite3').verbose();

// open database
let db = new sqlite3.Database('./db/concoursdeblog.db', sqlite3.OPEN_READWRITE, (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the Blog SQlite database.');
});

// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// *** DATA *** //
let formData = {
    id: null,
    title: '',
    thumbnail_url: '',
    info: '',
    author_name: "",
    date: ""
};

// Initialize the database first, only needed at first launch
initDB();
// initDB1()


// *** ROUTES *** //
/**
 * index page
 */
app.get('/', (req, res) => {
    res.render('pages/index');
});

/**
 * articles page
 */
app.get('/articles', (req, res) => {
    renderArticles(res)
    const query = `SELECT * FROM articles`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error(err.message);
        }

        res.render('pages/articles', {
            articles: rows
        });
    });
});





/**
 * new article page
 */
app.get('/new_article', (req, res) => {
    res.render('pages/new_article');
});


// app.post('/articles', (req, res) => {

// })

// *** CRUD *** //
/**
 * create articles
 */
app.post('/articles/create', (req, res) => {
    const query = `INSERT INTO articles (title, thumbnail_url, info, author_name, date)
        VALUES ('${req.body.title}', 
            '${req.body.image}',
            '${req.body.description}',
            '${req.body.author_name}',
            '${req.body.date}'
        )`;

    db.run(query, (err) => {
        if (err) {
            return console.error('ici: ', err.message);
        }
        console.log('Articles successfully created.');

        renderArticles(res);
    });
});


/**
 * view articles
 */
app.get('/articles/:id', (req, res) => {
    if (isNaN(req.params.id)) {
        return;
    }

    const query = `SELECT * FROM articles WHERE id = ${req.params.id}`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error(err.message);
        }

        if (!rows) {
            renderArticles(res);
            return;
        }

        const data = rows[0];

        formData = {
            id: null,
            title: '',
            thumbnail_url: '',
            info: '',
            author_name: "",
            date: ""
        }

        res.render('pages/articles', {
            articles: rows,
            formData: formData,
            showCreateArticles: false,
            showUpdateArticles: true
        });
    });
});




/**
 * Listen for connections
 */
app.listen(port, () => {
    console.log(`Blog app listening at http://localhost:${port}`)
});


/**
 * Create the table articles
 */
function initDB() {
    const query = `CREATE TABLE IF NOT EXISTS articles (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title VARCHAR(100) NOT NULL,
        thumbnail_url TEXT NOT NULL,
        info TEXT NOT NULL,
        author_name TEXT NOT NULL,
        /**
        * Create the table articles
        */
        date TEXT NOT NULL
    )`;



    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Table articles successfully created.');
        populateDB();




    });
}

/**
 * Add default articles to the table articles
 */
function populateDB() {
    const query = `INSERT INTO articles (id, title, thumbnail_url, info,author_name, date) VALUES
    (1, 'Mrs. Bridge',"https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam facilisis fringilla purus, in luctus ex tristique a. Phasellus non pellentesque tellus. ", 'Evan S. Connell', "10-11-25"),
    (2, 'Mr. Bridge',"https://images.unsplash.com/photo-1610995602017-c9592ed683fa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1040&q=80","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam facilisis fringilla purus, in luctus ex tristique a. Phasellus non pellentesque tellus. ", 'Evan S. Connell', "10-11-25"),
    (3, 'L''ingénue libertine',"https://images.unsplash.com/photo-1552538962-40822613a09d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=630&q=80","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam facilisis fringilla purus, in luctus ex tristique a. Phasellus non pellentesque tellus. ", 'Colette', "10-11-25");`;
    db.run(query, (err) => {
        if (err) {
            return console.error(err.message);
        }
        console.log('Table articles successfully populated.');
    });
}

// *** UTILITIES *** //
function renderArticles(res, showCreateArticles = false) {
    resetFormData();

    if (showCreateArticles) {
        res.render('pages/articles', {
            articles: [],
            formData: formData,
            showCreateArticles: showCreateArticles,
            showUpdateArticles: false
        });

        return;
    }

    const query = `SELECT * FROM articles`;
    db.all(query, [], (err, rows) => {
        if (err) {
            console.error('here: ', err.message);
        }

        res.render('pages/articles', {
            articles: rows,
            formData: formData,
            showCreateArticles: showCreateArticles,
            showUpdateArticles: false
        });
    });
}

function resetFormData() {
    formData = {
        id: null,
        title: '',
        thumbnail_url: '',
        info: '',
        author_name: "",
        date: ""

    };
}